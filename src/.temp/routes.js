export default [
  {
    path: "/about",
    component: () => import(/* webpackChunkName: "page--src--pages--about-vue" */ "/Users/yoonsunlee/my-gridsome-site/src/pages/About.vue")
  },
  {
    name: "404",
    path: "/404",
    component: () => import(/* webpackChunkName: "page--node-modules--gridsome--app--pages--404-vue" */ "/Users/yoonsunlee/my-gridsome-site/node_modules/gridsome/app/pages/404.vue")
  },
  {
    path: "/better",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/yoonsunlee/my-gridsome-site/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/post-two",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/Users/yoonsunlee/my-gridsome-site/src/templates/Post.vue"),
    meta: {
      data: true
    }
  },
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "page--src--pages--index-vue" */ "/Users/yoonsunlee/my-gridsome-site/src/pages/Index.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/hacker/:id",
    component: () => import(/* webpackChunkName: "page--src--templates--hacker-info-vue" */ "/Users/yoonsunlee/my-gridsome-site/src/templates/HackerInfo.vue"),
    meta: {
      data: true
    }
  },
  {
    path: "/:year/:month/:day/:slug",
    component: () => import(/* webpackChunkName: "page--src--templates--word-press-post-vue" */ "/Users/yoonsunlee/my-gridsome-site/src/templates/WordPressPost.vue"),
    meta: {
      data: true
    }
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "page--node-modules--gridsome--app--pages--404-vue" */ "/Users/yoonsunlee/my-gridsome-site/node_modules/gridsome/app/pages/404.vue")
  }
]

